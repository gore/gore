# overrides for node packages, fixing elm-test

{ pkgs ? import ../pinned-nixpkgs.nix {}
, system ? builtins.currentSystem
, nodejs ? pkgs."nodejs-6_x"
, ...}:

let
  nodeModules = import ./node-modules.nix { inherit pkgs system nodejs ; };
in
nodeModules
#  // {
#   "elm-test-^0.18.9" = nodeModules."elm-test-^0.18.9".override (old: {
#     postInstall = let
#       binary =  pkgs.fetchurl (if pkgs.system == "x86_64-linux" then {
#           url = https://dl.bintray.com/elmlang/elm-test/0.18.9/linux-x64.tar.gz;
#           sha256 = "0prfrjh0bv4rf8xxjf8jdiy64i489v108fypax4y9zazhv8imnh8";
#         } else if pkgs.system == "x86_64-darwin" then {
#           url = https://dl.bintray.com/elmlang/elm-test/0.18.9/darwin-x64.tar.gz;
#           sha256 = "01aya87jx9ry7ryzzc9vvwfzw15h9abln502rkipr2g93bqp392m";
#         } else throw "unsupported platform");
#     in ''
#       tar -xzf ${binary}
#       cp elm-interface-to-json $out/lib/node_modules/elm-test/bin/elm-interface-to-json
#       ls -la $out/lib/node_modules/elm-test/bin/
#     ''+ (if pkgs.system != "x86_64-linux" then "" else ''
#       patchelf --set-interpreter ${pkgs.stdenv.glibc}/lib/ld-linux-x86-64.so.2 $out/lib/node_modules/elm-test/bin/elm-interface-to-json
#       patchelf --set-rpath ${pkgs.stdenv.glibc}/lib:${pkgs.gmp}/lib $out/lib/node_modules/elm-test/bin/elm-interface-to-json
#     '');
#   });
# }
