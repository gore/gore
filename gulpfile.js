path = require('path')
gulp = require('gulp')
gElm  = require('gulp-elm')
gExec = require('gulp-exec')
gPlumber = require('gulp-plumber')
gReplace = require('gulp-replace')
gWebserver = require('gulp-webserver')
gExtReplace = require('gulp-ext-replace')
gDevelopServer = require('gulp-develop-server')
gClosureCompiler = require('gulp-closure-compiler')


gulp.task('elm-init', gElm.init)

gulp.task('build:bridge', () => {
  gulp.src('src/Protocol.hs')
    .pipe(gPlumber(() => console.log('Failed.')))
    .pipe(gExec('runhaskell  <%= file.path %>', {pipeStdout: true}))
    .pipe(gReplace(/import Json.Decode exposing \(\(\:\=\)\)/g, ''))
    .pipe(gReplace(/import Set/g , ''))
    .pipe(gExtReplace('.elm'))
    .pipe(gulp.dest('out/elm/'))
    .pipe(gExec.reporter({stdout: false}))
})

gulp.task('build:server', ['elm-init', 'build:bridge'], () =>
  gulp.src('src/Server/Main.elm')
    .pipe(gPlumber())
    .pipe(gElm({debug: true, warn: true, filetype:"js"}))
    .pipe(gulp.dest('out/server'))
)

gulp.task('build:compiled-server', ['build:server'], () =>
  gulp.src('out/server/Main.js')
    .pipe(
      gClosureCompiler({
        compilerPath: process.ENV.closurecompiler + '/share/java/compiler.jar',
        fileName: 'out/server/Main.compiled.js',
        compilerFlags: {
          warning_level: 'QUIET',
          compilation_level: 'ADVANCED_OPTIMIZATIONS' }})))

gulp.task('build:client', ['elm-init', 'build:bridge'], () =>
  gulp.src('src/Client/Main.elm')
    .pipe(gPlumber())
    .pipe(gElm({debug: true, warn: true, filetype:"html"}))
    .pipe(gulp.dest('out/client'))
)

gulp.task('build:htmltests', ['elm-init'], () =>
  gulp.src('tests/HtmlTests.elm' )
    .pipe(gPlumber())
    .pipe(gElm({debug: false, warn: true, filetype:"html"}))
    .pipe(gulp.dest('out/client'))
)

gulp.task('server:game', ['build:server'], () =>
  gDevelopServer.listen( { path: 'server.js' } )
)

gulp.task('server:http', ['build:bridge', 'build:client', 'build:htmltests', 'server:game'], () =>
  gulp.src('out/client')
    .pipe(gWebserver(
      { livereload: true
      , host: '0.0.0.0'
      , fallback: 'Main.html'
      }
    ))
)

gulp.task('test:noerr', () => {
  gulp.src('.')
    .pipe(gPlumber(() => console.log('Failed.')))
    .pipe(gExec('FORCE_COLOR=yes node_modules/elm-test/bin/elm-test'))
    .pipe(gExec.reporter({err: false}))
})

gulp.task('start', ['server:http', 'test:noerr'], () => {
  gulp.watch(['out/**/*.elm', 'src/**/*.elm'], ['build:server', 'build:client', 'test:noerr', 'build:htmltests'])
  gulp.watch('tests/**/*.elm', ['test:noerr', 'build:htmltests'])
  gulp.watch('src/**/*.hs', ['build:bridge'])
  gulp.watch( [ 'server.js' ], gDevelopServer.restart );
  gulp.watch( [ 'out/server/**' ], gDevelopServer.restart );
})

gulp.task('test', ['build:bridge', 'build:server', 'build:client'], () => {
  gulp.src('.')
    .pipe(gExec('FORCE_COLOR=yes node_modules/elm-test/bin/elm-test'))
    .pipe(gExec.reporter())
})

gulp.task('default', ['start'])
