# NixOS module defining gore server

{ config, lib, pkgs, ... } :
let
  cfg = config.services.gore;
  backend = import ../backend.nix {};
  frontend = import ../frontend.nix {};
in {
  options.services.gore = {
    serverName = lib.mkOption {
      type = lib.types.string;
    };
    enableSSL = lib.mkOption {
      type = lib.types.bool;
      default = true;
    };
  };

  config = {
    networking.extraHosts = "127.0.0.1 ${cfg.serverName}";
    networking.firewall.allowedTCPPorts = [ 22 80 443 8080 ];

    environment.systemPackages = [ pkgs.rsync backend ];

    users.extraUsers.gore = { group = "gore"; };
    users.extraGroups.gore = {};

    systemd.services.gore = {
      description = "gore backend";
      wants = [ "network.target"];
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      restartIfChanged = true;
      preStart = ''
        mkdir -p /var/lib/gore
        chown -R gore. /var/lib/gore
      '';
      script = ''
        cd /var/lib/gore
        export GORE_PORT=8090
        export GORE_DB_PATH=/var/lib/gore/db
        ${backend}/bin/gore-backend
      '';
      serviceConfig = {
        User = "gore";
        Group = "gore";
        PermissionsStartOnly = true;
        Restart = "on-failure";
      };
    };

    services.nginx = {
      enable = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;

      virtualHosts."gore-static-${cfg.serverName}" = {
        serverName = "${cfg.serverName}";
        forceSSL = cfg.enableSSL;
        enableACME = cfg.enableSSL;
        locations."/" = {
          root = "${frontend}/public";
        };
      };
      virtualHosts."gore-websocket-${cfg.serverName}" = {
        serverName = "${cfg.serverName}";
        listen = [ { addr = "0.0.0.0"; port = 8080; ssl = cfg.enableSSL; }];
        forceSSL = cfg.enableSSL;
        enableACME = cfg.enableSSL;
        locations."/" = {
          proxyPass = "http://127.0.0.1:8090";
          proxyWebsockets = true;
        };
      };
    };
  };
}
