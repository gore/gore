let
  nixpkgs = import ../pinned-nixpkgs.nix {};
  env = "gore";
  domain = "example.com";
in import "${nixpkgs.path}/nixos/tests/make-test.nix" ({ pkgs, ... }: {
  name = "gore";

  machine =
    { config, pkgs, ... }:
    {
      imports = [ ./gore.nix "${nixpkgs.path}/nixos/tests/common/x11.nix" ];
      services.gore = {
        env = env;
        domain = domain;
        debug = true;
      };

      environment.systemPackages = [ pkgs.firefox pkgs.xdotool ];

      networking.extraHosts = "127.0.0.1 ${env}.${domain}";
    };

  enableOCR = true;

  testScript =
    ''
      $machine->waitForX;
      $machine->execute("xterm -e 'firefox http://${env}.${domain}' &");
      $machine->waitForWindow(qr/gore/);
      $machine->sleep(40); # wait until Firefox has finished loading the page
      $machine->succeed("xwininfo -root -tree | grep gore");
      $machine->waitForText(qr/Lobby/);
      $machine->screenshot("screen");
    '';
})
