# Nix expression defining the frontend derivation

{ pkgs ? import ./pinned-nixpkgs.nix {}, ghc ? pkgs.haskell.packages.ghc802, ...} :
let
  frontendTools = import ./frontend-tools.nix { inherit pkgs; };
  ghcWithElmBridge = ghc.ghcWithPackages (pkgs: with pkgs;
    [ elm-bridge ]);
in frontendTools.mkFrontend rec {
  name = "gore-frontend";
  src = ./.;
  nodejs = pkgs."nodejs-6_x";
  node_modules = import ./node-modules-override.nix { inherit nodejs; inherit pkgs; };
  elm_packages = import ./elm-package.nix;
  buildInputs = [ frontendTools.updateFrontendDeps ];
  propagatedBuildInputs = [ ghcWithElmBridge ] ++ (with pkgs; [ closurecompiler ]);
  closurecompiler = pkgs.closurecompiler;
  # node_modules."elm-test-^0.18.9"
}
