# Define a pinned nixpkgs version
{ pkgs ? import <nixpkgs> {}, ...} :
let
  pinned_pkgs_path = pkgs.fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs";
    rev = "3fe7cddc304abb86e61a750a4f807270c7ca7825";
    sha256 = "13z9whcylsr76z3npc8v8hxalli6jz9h3jv5z4b97cli1kp9y04k";
  };
in
  import pinned_pkgs_path {}
