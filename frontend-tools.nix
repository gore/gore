# Tools for building Elm-based frontends

{ pkgs ? import <nixpkgs> {}, ...} :
let
  elm2nix = pkgs.stdenv.mkDerivation {
    name = "elm2nix";
    buildInputs = [ pkgs.ruby ];
    buildCommand = ''
      mkdir -p $out/bin
      cp ${pkgs.path}/pkgs/development/compilers/elm/elm2nix.rb $out/bin/elm2nix
      sed -i -e "s|\"package.nix\"|\"elm-package.nix\"|" $out/bin/elm2nix
      sed -i -e "s|nix-prefetch-url|nix-prefetch-url|" $out/bin/elm2nix
      chmod +x $out/bin/elm2nix
      patchShebangs $out/bin
    '';
  };

  updateFrontendDeps = pkgs.writeScriptBin "update-frontend-deps" ''
    #!${pkgs.stdenv.shell}
    set -e
    echo; echo "Pinning node dependencies from ./package.json ..."; echo
    ${pkgs.nix}/bin/nix-instantiate --strict --json --eval --expr '
      (import <nixpkgs> {}).lib.mapAttrsToList
        (name : value : { "${"\${name}"}" = value; })
        (let json = builtins.fromJSON (builtins.readFile ./package.json);
        in json.dependencies // json.devDependencies)
    ' > node-modules.json
    ${pkgs.nodePackages.node2nix}/bin/node2nix \
      --composition node-modules.nix \
      --input node-modules.json \
      --output node-modules-generated.nix \
      --node-env node-env.nix \
      --flatten \
      --pkg-name nodejs-6_x
    ${pkgs.gnused}/bin/sed -i -e "s| sources.\"elm-0.18| #sources.\"elm-0.18|" node-modules-generated.nix
    ${pkgs.gnused}/bin/sed -i -e "s| name = \"elm-webpack-loader\";| dontNpmInstall = true; name = \"elm-webpack-loader\";|" node-modules-generated.nix
    ${pkgs.gnused}/bin/sed -i -e "s| name = \"elm-test\";| dontNpmInstall = true; name = \"elm-test\";|" node-modules-generated.nix

    # echo; echo "Updating tests/elm-package.json..."
    # ./node_modules/elm-test/bin/elm-test --add-dependencies tests/elm-package.json

    echo; echo "Pinning elm dependencies from ./elm-package.json"; echo
    rm -rf elm-stuff || true
    # rm -rf tests/elm-stuff || true
    # ln -s ../elm-stuff tests/elm-stuff
    n=0
    until [ $n -ge 3 ]
    do
      ${pkgs.elmPackages.elm}/bin/elm-package install -y || true
      n=$[$n+1]
      sleep 2
    done
    ${elm2nix}/bin/elm2nix

  '';

  makeElmStuff = deps:
    let
      inherit (pkgs) lib fetchurl;
      json = builtins.toJSON (lib.mapAttrs (name: info: info.version) deps);
      cmds = lib.mapAttrsToList (name: info: let
        pkg = pkgs.stdenv.mkDerivation {
          name = lib.replaceStrings ["/"] ["-"] name + "-${info.version}";
          src = fetchurl {
            url = "https://github.com/${name}/archive/${info.version}.tar.gz";
            meta.homepage = "https://github.com/${name}/";
            inherit (info) sha256;
          };
          phases = [ "unpackPhase" "installPhase" ];
          installPhase = ''
            mkdir -p $out
            cp -r * $out
            '';
          };
        in ''
          mkdir -p elm-stuff/packages/${name}
          ln -s ${pkg} elm-stuff/packages/${name}/${info.version}
        '') deps;
    in ''
      home_old=$HOME
      HOME=/tmp
      mkdir elm-stuff
      cat > elm-stuff/exact-dependencies.json <<EOF
      ${json}
      EOF
      ${lib.concatStrings cmds}
      HOME=$home_old
    '';

  mkFrontend =
    allArgs@
    { src
    , nodejs
    , node_modules
    , elm_packages
    , buildInputs ? []
    , buildPhase ? ""
    , installPhase ? ""
    , shellHook ? ""
    , ...
    }:
    let
      args = removeAttrs allArgs ["nodejs" "node_modules" "elm_packages"];
      self = pkgs.stdenv.mkDerivation (args // {
        src =
          builtins.filterSource (path: type:
            baseNameOf path != "elm-stuff" && baseNameOf path != "node_modules"
          ) src;

        buildInputs = buildInputs ++ [ nodejs pkgs.elmPackages.elm ] ++ (builtins.attrValues node_modules);

        configurePhase = ''
          set -e
          rm -rf node_modules || true
          mkdir node_modules
          for item in ${builtins.concatStringsSep " " (builtins.attrValues node_modules)}; do
            ln -s $item/lib/node_modules/* ./node_modules
          done

          export NODE_PATH=$PWD/node_modules:$NODE_PATH

          rm -rf elm-stuff
        '' + makeElmStuff elm_packages;

        buildPhase = if buildPhase != "" then buildPhase else "gulp build:client";

        doCheck = true;

        checkPhase = ''
          echo; echo "Running ... elm-format-0.18 src/ --validate"
          elm-format-0.18 src/ --validate
          echo; echo "Running ... elm-format-0.18 tests/ --validate"
          elm-format-0.18 tests/ --validate
          # echo; echo "Running ... elm-test"
          # ./node_modules/elm-test/bin/elm-test
        '';

        installPhase = if installPhase != "" then installPhase else ''
          mkdir -p $out/public
          cp -R out/client/Main.html $out/public/index.html
          runHook postInstall
        '';

        shellHook = ''
          set +e
          ${self.configurePhase}
          ${shellHook}
          set +e
        '';
      });
    in self;
in
  {
    inherit elm2nix;
    inherit mkFrontend;
    inherit updateFrontendDeps;
  }
