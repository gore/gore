module Tests exposing (..)

import Test exposing (..)
import Expect
import Client.Model exposing (init)


all : Test
all =
    describe "Test Suite"
        [ describe "Model Tests"
            [ test "Initial user is Nothing" <|
                \() ->
                    Expect.equal (init "").user Nothing
            ]
        ]
