___ = (label, arg) => { console.log(label, arg); return arg }
WS = require('ws')
fs = require('fs')
readline = require('readline')
Cookie = require('cookie')
Elm = require('./out/server/Main.js')
Http = require('http')
Crypto = require('crypto')

i = 0

httpServer = Http.createServer()
httpServer.on('upgrade', (request, socket, head) => {
    request.SESSIONID = Cookie.parse(request.headers['cookie'] || "")['SESSIONID'] || (Crypto.randomBytes(8).toString('hex'))
    socket.write(
        'HTTP/1.1 101 Switching Protocols\r\nSet-Cookie:SESSIONID='+request.SESSIONID+'\r\nOld-Header:')
})
httpServer.listen(process.env.GORE_PORT || 8080)

ws = new WS.Server({server: httpServer})

elm_worker = Elm.Server.Main.worker()

cs = {}

const rl = readline.createInterface({
    input: fs.createReadStream(process.env.GORE_DB_PATH || 'db', { flags: 'a+'})
})

rl.on('line', (line) => {
    //console.log("read db: ", line)
    event = JSON.parse(line)
    elm_worker.ports.message.send(["", event[0], '{"GoreEvent": ' + JSON.stringify(event[1]) + '}'])
})

ws.on('connection', (conn) => {
    i++;
    conn_id = i + '';
    session_id = conn.upgradeReq.SESSIONID
    console.log('connection', conn_id, session_id)
    conn.send(___(conn_id+' <--- ', '{"LoginMsg":"'+session_id+'"}'))
    elm_worker.ports.connect.send([conn_id+'', session_id+'']);
    cs[conn_id] = conn
    conn.on('close', ((conn_id, session_id) => () => {
        delete cs[session_id]
        elm_worker.ports.close.send([conn_id+'', session_id+''])
    })(conn_id, session_id))
    conn.on('message', ((conn_id, session_id) => (message) =>
        elm_worker.ports.message.send([conn_id+'', session_id+'', ___(conn_id+' ---> ', message)])
    )(conn_id, session_id))
})

elm_worker.ports.out.subscribe(([conn_ids, strs]) => {
    conn_ids.forEach((conn_id) => {
        strs.forEach((str) => {
            try {
                cs[conn_id*1].send(___(conn_id+' <--- ', str))
            } catch(e) {
                console.error('error while sending: ', conn_id, e)
            }
        })
    })
})

elm_worker.ports.persist.subscribe(([user_id, str]) => {
    var data = '["' + user_id + '", ' + str + ']\n'
    fs.appendFile('db', data, (err) => { if (err) { throw err } })
})
