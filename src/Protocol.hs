{-# LANGUAGE TemplateHaskell #-}
import Elm.Derive
import Elm.Module

import Data.Proxy


type UserId = String
type TableId = String
type ChatId = String


data EngineEvent
    = PlaceStone Int Int
    | Pass
    deriving (Show, Eq)


data TableEvent
    = Spectate
    | TakeBlack
    | TakeWhite
    | Leave
    | Play EngineEvent
    deriving (Show, Eq)


data GoreEvent
    = TableMsg TableId ( UserId, TableEvent )
    | ChatMsg ChatId Int
    | LoginMsg UserId
    deriving (Show, Eq)


data Query
    = Lobby
    | Table TableId
    | Chat ChatId
    deriving (Show, Eq)


data SubscriptionEvent
    = Subscribe Query
    | Unsubscribe Query
    | Disconnect
    deriving (Show, Eq)


data ClientToServer
    = GoreEvent GoreEvent
    | SubscriptionMsg SubscriptionEvent
    deriving (Show, Eq)


deriveBoth defaultOptions ''EngineEvent
deriveBoth defaultOptions ''TableEvent
deriveBoth defaultOptions ''GoreEvent
deriveBoth defaultOptions ''Query
deriveBoth defaultOptions ''SubscriptionEvent
deriveBoth defaultOptions ''ClientToServer


elm :: String
elm =
  makeElmModule "Protocol"
    [ DefineElm (Proxy :: Proxy EngineEvent)
    , DefineElm (Proxy :: Proxy TableEvent)
    , DefineElm (Proxy :: Proxy GoreEvent)
    , DefineElm (Proxy :: Proxy Query)
    , DefineElm (Proxy :: Proxy SubscriptionEvent)
    , DefineElm (Proxy :: Proxy ClientToServer)
    ]



string_alias :: String -> String
string_alias alias = unlines
    [ ""
    , "type alias " ++ alias ++ " = String"
    , ""
    , ""
    , "jsonEnc" ++ alias ++ " : " ++ alias ++ " -> Json.Encode.Value"
    , "jsonEnc" ++ alias ++ " = Json.Encode.string"
    , ""
    , ""
    , "jsonDec" ++ alias ++ " : Json.Decode.Decoder " ++ alias
    , "jsonDec" ++ alias ++ " = Json.Decode.string"
    ]


main :: IO ()
main = do
    putStrLn elm
    putStrLn (string_alias "UserId")
    putStrLn (string_alias "TableId")
    putStrLn (string_alias "ChatId")
