module LobbyClient.View exposing (..)

import Dict
import Html exposing (..)
import Html.Attributes exposing (..)
import Maybe exposing (withDefault)
import Table.Model
import Client.Elements exposing (..)
import Gore
import Protocol


type alias LobbyEvent =
    ( Protocol.TableId, Protocol.TableEvent )


view : Protocol.UserId -> Gore.GoreModel -> Html LobbyEvent
view user gore =
    div [ class "container" ] [ view_lobby user gore ]


view_lobby : Protocol.UserId -> Gore.GoreModel -> Html LobbyEvent
view_lobby user gore =
    div []
        [ p [] [ text "Let's All Go To The Lobby" ]
        , btn ( "", Protocol.TakeBlack ) "Create Table Black"
        , btn ( "", Protocol.TakeWhite ) "Create Table White"
        , btn ( "", Protocol.Spectate ) "Create Table"
        , table []
            [ thead [] <|
                List.map (th []) <|
                    List.map List.singleton <|
                        List.map text <|
                            [ "Table", "Black Player", "White Player" ]
            , tbody [] <| Dict.values <| Dict.map view_table gore.tables
            ]
        ]


view_slot : LobbyEvent -> Maybe Protocol.UserId -> Html LobbyEvent
view_slot event user =
    td []
        [ user
            |> Maybe.map text
            |> withDefault (btn event "Join")
        ]


view_table : Protocol.TableId -> Table.Model.Model -> Html LobbyEvent
view_table table_id table =
    tr []
        [ td [] [ text <| table_id ]
        , view_slot ( table_id, Protocol.TakeBlack ) table.black_user
        , view_slot ( table_id, Protocol.TakeWhite ) table.white_user
        , td [] [ btn ( table_id, Protocol.Spectate ) "spectate" ]
        ]


bordered : Attribute msg
bordered =
    style [ ( "border", "1px solid black" ) ]
