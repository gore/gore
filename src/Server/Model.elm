module Server.Model exposing (..)

import Dict exposing (Dict)
import Table.Model
import Subscription exposing (SubscriptionModel)
import Protocol


type alias Tables =
    Dict Protocol.TableId Table.Model.Model


type alias Model =
    { subscriptions : SubscriptionModel
    , tables : Tables
    , events : List Protocol.GoreEvent
    }


init : Model
init =
    { subscriptions = Subscription.init
    , tables = Dict.empty
    , events = []
    }
