module Server.Update exposing (..)

import Dict
import EveryDict
import Either exposing (Either(..))
import Server.Model exposing (Model, Tables)
import Table.Update
import Table.Model
import Subscription
import Protocol exposing (UserId, TableId)


type alias ConnId =
    String


type Event
    = Connect
    | ClientMessage (Result String Protocol.ClientToServer)
    | Close


type alias Msg =
    ( ConnId, UserId, Event )


type alias OutMsg =
    ( ConnId, Protocol.GoreEvent )


validate_tables : UserId -> TableId -> ( UserId, Protocol.TableEvent ) -> Tables -> Bool
validate_tables user_id table_id msg tables =
    tables
        |> Dict.get table_id
        |> (\m_table ->
                case m_table of
                    Just table ->
                        Table.Update.validate msg table

                    Nothing ->
                        Table.Update.validate msg Table.Model.init
           )


update_tables : UserId -> TableId -> ( UserId, Protocol.TableEvent ) -> Tables -> Tables
update_tables user_id table_id msg tables =
    tables
        |> Dict.update table_id
            (\m_table ->
                case m_table of
                    Just table ->
                        if Table.Update.validate msg table then
                            Table.Update.update msg table
                        else
                            Just table

                    Nothing ->
                        if Table.Update.validate msg Table.Model.init then
                            Table.Update.update msg Table.Model.init
                        else
                            Nothing
            )


update_gore : ConnId -> UserId -> Protocol.GoreEvent -> Model -> Maybe Model
update_gore conn_id user_id gore_msg model =
    case gore_msg of
        Protocol.TableMsg table_id table_msg ->
            if validate_tables user_id table_id table_msg model.tables then
                let
                    tables_ =
                        update_tables user_id table_id table_msg model.tables
                in
                    Just
                        { model
                            | tables = tables_
                            , events = gore_msg :: model.events
                        }
            else
                always Nothing <| Debug.log "not valid" gore_msg

        _ ->
            Nothing


update_subscriptions : ConnId -> UserId -> Protocol.SubscriptionEvent -> Model -> ( Model, List Protocol.GoreEvent )
update_subscriptions conn_id user_id subscription_msg model =
    ( { model
        | subscriptions = Subscription.update conn_id subscription_msg model.subscriptions
      }
    , case subscription_msg of
        Protocol.Subscribe q ->
            model.events |> List.filter (Subscription.test q)

        _ ->
            []
    )


update_cts : ConnId -> UserId -> Protocol.ClientToServer -> Model -> ( Model, Either (Maybe (List ConnId)) (List Protocol.GoreEvent) )
update_cts conn_id user_id cts model =
    case cts of
        Protocol.GoreEvent gore_msg ->
            case update_gore conn_id user_id gore_msg model of
                Just model_ ->
                    let
                        conns =
                            Subscription.dispatch gore_msg model.subscriptions |> EveryDict.keys
                    in
                        ( model_, Left (Just conns) )

                Nothing ->
                    ( model, Left Nothing )

        Protocol.SubscriptionMsg subscription_msg ->
            let
                ( model_, events ) =
                    update_subscriptions conn_id user_id subscription_msg model
            in
                ( model_, Right events )


update : Msg -> Model -> ( Model, Either (Maybe (List ConnId)) (List Protocol.GoreEvent) )
update ( conn_id, user_id, event ) model =
    case event of
        Connect ->
            ( model, Right [] )

        -- TODO
        ClientMessage (Ok cts) ->
            update_cts conn_id user_id cts model

        ClientMessage (Err err) ->
            Debug.log "ClientMessage Err" err |> always ( model, Right [] )

        Close ->
            update_cts conn_id user_id (Protocol.SubscriptionMsg Protocol.Disconnect) model
