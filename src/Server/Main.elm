port module Server.Main exposing (main)

import Json.Decode
import Json.Encode
import Platform
import Either exposing (Either(..))
import Server.Model exposing (Model, init)
import Server.Update exposing (Msg, Event(..))
import Protocol


type alias Value =
    Json.Decode.Value


type alias ConnId =
    String


port connect : (( ConnId, Protocol.UserId ) -> msg) -> Sub msg


port message : (( ConnId, Protocol.UserId, String ) -> msg) -> Sub msg


port close : (( ConnId, Protocol.UserId ) -> msg) -> Sub msg


port out : ( List ConnId, List String ) -> Cmd msg


port persist : ( Protocol.UserId, String ) -> Cmd msg


enc : Protocol.GoreEvent -> String
enc event =
    event
        |> Protocol.jsonEncGoreEvent
        |> Json.Encode.encode 0


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        ( conn_id, user_id, event ) =
            msg

        ( model_, result ) =
            Server.Update.update msg model
    in
        ( model_
        , case result of
            Left (Just conn_ids) ->
                case event of
                    ClientMessage (Ok (Protocol.GoreEvent gore_event)) ->
                        Cmd.batch
                            [ out ( conn_ids, [ enc gore_event ] )
                            , persist ( user_id, enc gore_event )
                            ]

                    _ ->
                        Cmd.none

            Left Nothing ->
                Cmd.none

            Right events ->
                out ( [ conn_id ], events |> List.reverse |> List.map enc )
        )


main : Program Never Model Msg
main =
    Platform.program
        { init = ( init, Cmd.none )
        , update = update
        , subscriptions =
            always <|
                Sub.batch
                    [ connect (\( c, u ) -> ( c, u, Connect ))
                    , message
                        (\( c, u, mstr ) ->
                            ( c, u, ClientMessage (Json.Decode.decodeString Protocol.jsonDecClientToServer mstr) )
                        )
                    , close (\( c, u ) -> ( c, u, Close ))
                    ]
        }
