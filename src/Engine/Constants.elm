module Engine.Constants exposing (..)


grid_size : Int
grid_size =
    19


grid_spacing : Float
grid_spacing =
    20
