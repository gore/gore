module Engine.Update exposing (update)

import Array
import Engine.Model as Model
import Protocol exposing (EngineEvent(..))


type alias Point =
    { row : Int, col : Int }


type InvalidAction
    = Ko
    | Suicide
    | Occupied



-- get the valid neighbours of the current point


getNeighbours : Model.Board -> Point -> List Point
getNeighbours board point =
    let
        up =
            { point | row = point.row - 1 }

        right =
            { point | col = point.col + 1 }

        down =
            { point | row = point.row + 1 }

        left =
            { point | col = point.col - 1 }

        height =
            Array.length board

        width =
            Array.length (Maybe.withDefault Array.empty (Array.get 0 board))

        valid neighbour =
            (neighbour.row >= 0 && neighbour.row < height)
                && (neighbour.col >= 0 && neighbour.col < width)
    in
        [ up, right, down, left ] |> List.filter valid



-- calculate the liberties of the group of a color at this point
-- NOTE: This function *can* currently count liberties multiple times,
--       but it doesn't matter, because we only care if it's zero.


getLiberties : Model.Board -> Point -> Model.Color -> Int
getLiberties board point color =
    let
        pointColor =
            getCell point board

        visitedBoard =
            updateCell point (otherColor color) board

        combine neighbour liberties =
            liberties + getLiberties visitedBoard neighbour color
    in
        if pointColor == otherColor color then
            0
        else if pointColor == Model.Empty then
            1
        else
            getNeighbours board point |> List.foldl combine 0



-- find and remove all of the stones connected to this one


removeGroup : Model.Board -> Point -> Int -> ( Model.Board, Int )
removeGroup board point removedCount =
    let
        thisColor =
            getCell point board

        neighbours =
            getNeighbours board point

        nextBoard =
            updateCell point Model.Empty board

        combine neighbour ( prevBoard, prevCount ) =
            if (getCell neighbour prevBoard == thisColor) then
                removeGroup prevBoard neighbour prevCount
            else
                ( prevBoard, prevCount )
    in
        neighbours |> List.foldl combine ( nextBoard, removedCount + 1 )


capture : List Point -> Model.Color -> Model.Board -> ( Model.Board, Int )
capture neighbours player board =
    let
        opponent =
            otherColor player

        combine neighbour ( prevBoard, prevCount ) =
            let
                neighbourColor =
                    getCell neighbour prevBoard

                neighbourLiberties =
                    getLiberties prevBoard neighbour opponent
            in
                if neighbourColor == opponent && neighbourLiberties == 0 then
                    removeGroup prevBoard neighbour prevCount
                else
                    ( prevBoard, prevCount )
    in
        neighbours |> List.foldl combine ( board, 0 )


addCaptures : Model.Color -> { black : Int, white : Int } -> Int -> { black : Int, white : Int }
addCaptures who captures captureCount =
    case who of
        Model.Black ->
            { captures | black = captures.black + captureCount }

        Model.White ->
            { captures | white = captures.white + captureCount }

        _ ->
            captures


getCell : Point -> Model.Board -> Model.Color
getCell { row, col } board =
    (Array.get row board)
        |> (Maybe.withDefault Array.empty)
        |> Array.get col
        |> (Maybe.withDefault Model.Empty)


updateCell : Point -> Model.Color -> Model.Board -> Model.Board
updateCell { row, col } color board =
    let
        rowContents =
            Maybe.withDefault Array.empty (Array.get row board)
    in
        Array.set row (Array.set col color rowContents) board


placeStone : Point -> Model.Model -> Result InvalidAction Model.Model
placeStone point model =
    let
        pointState =
            getCell point model.board

        tryStone =
            { model
                | board = updateCell point model.turn model.board
                , turn = otherColor model.turn
            }

        ( removedDeadStones, captureCount ) =
            capture (getNeighbours tryStone.board point) model.turn tryStone.board

        liberties =
            getLiberties removedDeadStones point model.turn

        final =
            { tryStone
                | board = removedDeadStones
                , captures = addCaptures model.turn model.captures captureCount
                , history = model.board :: model.history
                , lastMove = Just ( point.row, point.col )
            }
    in
        if pointState /= Model.Empty then
            Err Occupied
        else if liberties == 0 then
            Err Suicide
        else if List.head model.history == Just final.board then
            Err Ko
        else
            Ok final


otherColor : Model.Color -> Model.Color
otherColor color =
    case color of
        Model.Black ->
            Model.White

        Model.White ->
            Model.Black

        Model.Empty ->
            Model.Empty



-- TODO: remove


updateResult : EngineEvent -> Model.Model -> Result InvalidAction Model.Model
updateResult action model =
    case action of
        PlaceStone row col ->
            placeStone { row = row, col = col } model

        Pass ->
            Ok { model | turn = otherColor model.turn }


update : EngineEvent -> Model.Model -> Model.Model
update action model =
    let
        result =
            updateResult action model
    in
        case result of
            Ok model ->
                model

            Err err ->
                Debug.log (toString err) model
