module Engine.Components.TurnIndicator exposing (turnIndicator)

import Html as H
import Html.Attributes as A
import Engine.Components.Stone exposing (stone)
import Engine.Model as Model
import Protocol


type alias Props =
    { color : Model.Color
    , size : Float
    }


turnIndicator : Props -> H.Html Protocol.EngineEvent
turnIndicator props =
    H.div
        [ A.style
            [ ( "display", "inline-block" )
            , ( "height", toString props.size ++ "px" )
            , ( "position", "relative" )
            , ( "width", toString props.size ++ "px" )
            ]
        ]
        [ stone { color = props.color, containerWidth = props.size }
        ]
