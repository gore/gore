module Engine.Components.Intersection exposing (intersection)

import Html as H
import Html.Attributes as A
import Html.Events as E
import Engine.Constants as C
import Engine.Components.Stone exposing (stone)
import Engine.Model as Model
import Protocol


type alias Props =
    { col : Int
    , row : Int
    , isTopEdge : Bool
    , isRightEdge : Bool
    , isLeftEdge : Bool
    , isBottomEdge : Bool
    , isStarPoint : Bool
    , state : Model.Color
    , width : Float
    , isLastMove : Bool
    }


intersection : Props -> H.Html Protocol.EngineEvent
intersection props =
    -- Layout
    H.div
        [ A.style
            [ ( "float", "left" )
            , ( "lineHeight", "1" )
            , ( "paddingTop", toString props.width ++ "%" )
            , ( "position", "relative" )
            , ( "textAlign", "center" )
            , ( "width", toString props.width ++ "%" )
            ]
        ]
        [ -- Wrapper
          H.div
            [ E.onClick (Protocol.PlaceStone props.row props.col)
            , A.style
                [ ( "cursor"
                  , (if props.state == Model.Empty then
                        "pointer"
                     else
                        "auto"
                    )
                  )
                , ( "position", "absolute" )
                , ( "top", "0" )
                , ( "right", "0" )
                , ( "bottom", "0" )
                , ( "left", "0" )
                ]
            ]
            [ -- Star Point Marker
              (if props.isStarPoint then
                H.div
                    [ A.style
                        [ ( "background", "#63380E" )
                        , ( "border-radius", "9000px" )
                        , ( "height", "6px" )
                        , ( "position", "absolute" )
                        , ( "left", "50%" )
                        , ( "top", "50%" )
                        , ( "transform", "translate(-50%, -50%)" )
                        , ( "width", "6px" )
                        ]
                    ]
                    []
               else
                H.text ""
              )
            , -- Grid Lines
              H.div
                [ A.style
                    [ ( "background", "#63380E" )
                    , ( "height", "1px" )
                    , ( "position", "absolute" )
                    , ( "top", "50%" )
                    , ( "right"
                      , if props.isRightEdge then
                            "50%"
                        else
                            "0"
                      )
                    , ( "left"
                      , if props.isLeftEdge then
                            "50%"
                        else
                            "0"
                      )
                    , ( "transform", "translateY(-50%)" )
                    ]
                ]
                []
            , H.div
                [ A.style
                    [ ( "background", "#63380E" )
                    , ( "position", "absolute" )
                    , ( "left", "50%" )
                    , ( "top"
                      , if props.isTopEdge then
                            "50%"
                        else
                            "0"
                      )
                    , ( "bottom"
                      , if props.isBottomEdge then
                            "50%"
                        else
                            "0"
                      )
                    , ( "transform", "translateX(-50%)" )
                    , ( "width", "1px" )
                    ]
                ]
                []
            , -- Render a stone if someone played here.
              case props.state of
                Model.Black ->
                    stone { color = Model.Black, containerWidth = C.grid_spacing }

                Model.White ->
                    stone { color = Model.White, containerWidth = C.grid_spacing }

                Model.Empty ->
                    H.text ""
            , if props.isLastMove then
                lastMoveMarker
              else
                H.text ""
            ]
        ]


lastMoveMarker : H.Html msg
lastMoveMarker =
    H.div
        [ A.style
            [ ( "background", "#55B5DB" )
            , ( "border-radius", "9000px" )
            , ( "height", "25%" )
            , ( "width", "25%" )
            , ( "position", "absolute" )
            , ( "left", "50%" )
            , ( "top", "50%" )
            , ( "transform", "translate(-50%, -50%)" )
            ]
        ]
        []
