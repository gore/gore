module Engine.Components.PassButton exposing (passButton)

import Html as H
import Html.Attributes as A
import Html.Events as E
import Protocol


type alias Props =
    {}


passButton : Props -> H.Html Protocol.EngineEvent
passButton props =
    H.button
        [ E.onClick Protocol.Pass
        , A.class "waves-effect waves-light btn blue-grey darken-4"
        ]
        [ H.text "Pass"
        ]
