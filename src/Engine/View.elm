module Engine.View exposing (view)

import Html as H
import Html.Attributes as A
import Engine.Model as Model
import Engine.Components.Board exposing (board)
import Engine.Components.ControlPanel exposing (controlPanel)
import Protocol


view : Model.Model -> H.Html Protocol.EngineEvent
view model =
    H.div []
        [ H.div [ A.class "container" ]
            [ board { boardState = model.board, lastMove = model.lastMove }
            ]
        , H.div
            [ A.class "page-footer blue-grey darken-2"
            , A.style [ ( "marginTop", "20px" ), ( "padding", "20px" ) ]
            ]
            [ H.div [ A.class "container" ]
                [ controlPanel
                    { captures = model.captures
                    , turn = model.turn
                    }
                ]
            ]
        ]
