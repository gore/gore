module Gore exposing (..)

import Dict exposing (Dict)
import Maybe.Extra exposing ((?))
import Table.Update
import Table.Model
import Protocol exposing (GoreEvent(..), TableId)


type alias GoreModel =
    { tables : Dict TableId Table.Model.Model
    }


update_gore : GoreEvent -> GoreModel -> GoreModel
update_gore event model =
    case event of
        TableMsg table_id table_msg ->
            let
                tables =
                    model.tables
                        |> Dict.update table_id
                            (\m_table ->
                                case m_table of
                                    Just table ->
                                        Just (Table.Update.update table_msg table ? table)

                                    Nothing ->
                                        Table.Update.update table_msg Table.Model.init
                            )
            in
                { tables = tables }

        _ ->
            model
