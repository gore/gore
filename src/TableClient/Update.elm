module TableClient.Update exposing (..)

import Table.Model exposing (Model, init, user_to_move)
import Table.Update
import Protocol


type Event
    = ServerMsg ( Protocol.UserId, Protocol.TableEvent )
    | ViewMsg Protocol.TableEvent
    | EngineMsg Protocol.EngineEvent


update : Protocol.UserId -> Event -> Model -> ( Model, Maybe Protocol.TableEvent )
update me msg model =
    case msg of
        ServerMsg table_msg ->
            case Table.Update.update table_msg model of
                Just new_model ->
                    ( new_model, Nothing )

                Nothing ->
                    --TODO: server sent invalid move
                    ( model, Nothing )

        ViewMsg table_event ->
            ( model, Just table_event )

        EngineMsg player_action ->
            if Just me /= user_to_move model then
                ( model, Nothing )
            else
                case Table.Update.update ( me, Protocol.Play player_action ) model of
                    Just new_model ->
                        ( new_model, Just (Protocol.Play player_action) )

                    Nothing ->
                        --TODO: user made invalid move
                        ( model, Nothing )
