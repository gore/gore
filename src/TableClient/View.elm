module TableClient.View exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Maybe.Extra exposing ((?))
import Engine.View
import Engine.Model
import Engine.Components.TurnIndicator
import Table.Model exposing (Model)
import TableClient.Update exposing (Event(..))
import Client.Elements exposing (..)
import Protocol


view : Protocol.UserId -> Model -> Html Event
view user model =
    div []
        [ div [ class "blue-grey darken-2" ]
            [ div [ class "container", style [ ( "padding", "20px" ) ] ]
                [ view_header "TODO:table_id"
                , view_color user model
                ]
            ]
        , Engine.View.view model.engine |> Html.map EngineMsg
        ]


view_header : Protocol.TableId -> Html Event
view_header table_id =
    div []
        [ text ("Table: " ++ table_id)
        , div [ style [ ( "float", "right" ) ] ]
            [ btn (ViewMsg Protocol.Leave) "Leave" ]
        ]


view_color : Protocol.UserId -> Model -> Html Event
view_color user model =
    let
        color =
            if Just user == model.black_user then
                Engine.Model.Black
            else if Just user == model.white_user then
                Engine.Model.White
            else
                Engine.Model.Empty
    in
        div []
            [ Engine.Components.TurnIndicator.turnIndicator { color = Engine.Model.Black, size = 20.0 } |> Html.map EngineMsg
            , text (model.black_user ? "-")
            , text
                (if Just user == model.black_user then
                    " (YOU)"
                 else
                    ""
                )
            , div [] []
            , Engine.Components.TurnIndicator.turnIndicator { color = Engine.Model.White, size = 20.0 } |> Html.map EngineMsg
            , text (model.white_user ? "-")
            , text
                (if Just user == model.white_user then
                    " (YOU)"
                 else
                    ""
                )
            ]
