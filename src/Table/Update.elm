module Table.Update exposing (..)

import Engine.Update
import Engine.Model
import Table.Model as Model exposing (..)
import Protocol


validate_action : ( Protocol.UserId, Protocol.EngineEvent ) -> Model -> Bool
validate_action ( user, action ) model =
    let
        users_turn =
            model.engine.turn
                == Engine.Model.Black
                && model.black_user
                == Just user
                || model.engine.turn
                == Engine.Model.White
                && model.white_user
                == Just user

        new_model =
            { model | engine = model.engine |> Engine.Update.update action }
    in
        if users_turn && model /= new_model then
            True
        else
            False


validate : ( Protocol.UserId, Protocol.TableEvent ) -> Model -> Bool
validate ( user, event ) model =
    case event of
        Protocol.Play action ->
            validate_action ( user, action ) model

        Protocol.Spectate ->
            True

        Protocol.TakeBlack ->
            model.black_user == Nothing

        Protocol.TakeWhite ->
            model.white_user == Nothing

        Protocol.Leave ->
            True


update : ( Protocol.UserId, Protocol.TableEvent ) -> Model -> Maybe Model
update ( user, event ) model =
    if not (validate ( user, event ) model) then
        Nothing
    else
        Just <|
            case event of
                Protocol.Spectate ->
                    model

                Protocol.Play action ->
                    { model | engine = model.engine |> Engine.Update.update action }

                Protocol.TakeBlack ->
                    { model | black_user = Just user }

                Protocol.TakeWhite ->
                    { model | white_user = Just user }

                Protocol.Leave ->
                    model
