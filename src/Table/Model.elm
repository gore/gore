module Table.Model exposing (..)

import Protocol
import Engine.Model


type alias Model =
    { black_user : Maybe Protocol.UserId
    , white_user : Maybe Protocol.UserId
    , history : List ( Protocol.UserId, Protocol.TableEvent )
    , engine : Engine.Model.Model
    }


init : Model
init =
    { black_user = Nothing
    , white_user = Nothing
    , history = []
    , engine = Engine.Model.init
    }


user_to_move : Model -> Maybe Protocol.UserId
user_to_move model =
    case model.engine.turn of
        Engine.Model.Black ->
            model.black_user

        Engine.Model.White ->
            model.white_user

        _ ->
            Nothing
