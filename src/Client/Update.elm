module Client.Update exposing (..)

import WebSocket
import Either exposing (Either(..))
import Json.Encode
import Maybe.Extra exposing (maybeToList)
import Dict
import Random
import Client.Model as Model exposing (Model, State(..), init)
import TableClient.Update
import Gore exposing (update_gore)
import Protocol exposing (GoreEvent(..))


type alias LobbyEvent =
    ( Protocol.TableId, Protocol.TableEvent )


type Msg
    = ServerMsg (Result String Protocol.GoreEvent)
    | ViewMsg (Either LobbyEvent TableClient.Update.Event)
    | Nop


send : String -> Protocol.ClientToServer -> Cmd Msg
send addr =
    Protocol.jsonEncClientToServer
        >> Json.Encode.encode 0
        >> WebSocket.send addr


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        send_msgs emsgs =
            emsgs |> List.map (send model.ws_addr) |> Cmd.batch
    in
        case ( msg, model.state, model.user ) of
            ( ServerMsg (Ok (Protocol.LoginMsg user_id)), _, _ ) ->
                ( { model | user = Just user_id, state = Lobby }
                , send_msgs [ Protocol.SubscriptionMsg (Protocol.Subscribe Protocol.Lobby) ]
                )

            ( ServerMsg (Ok gore_event), _, _ ) ->
                ( { model | gore = update_gore gore_event model.gore }
                , Cmd.none
                )

            ( ViewMsg (Left ( "", table_event )), Lobby, Just me ) ->
                ( model
                , (Random.int 1000000000000000 9007199254740991)
                    |> Random.generate
                        (\table_id ->
                            (ViewMsg (Left ( (toString table_id), table_event )))
                        )
                )

            ( ViewMsg (Left ( table_id, table_event )), Lobby, Just me ) ->
                ( { model | state = Table table_id }
                , send_msgs
                    [ Protocol.GoreEvent (TableMsg table_id ( me, table_event ))
                    , Protocol.SubscriptionMsg (Protocol.Subscribe (Protocol.Table table_id))
                    ]
                )

            ( ViewMsg (Right (TableClient.Update.ViewMsg (Protocol.Leave))), Table table_id, Just me ) ->
                ( { model | state = Lobby }
                , send_msgs
                    [ Protocol.GoreEvent (TableMsg table_id ( me, Protocol.Leave ))
                    , Protocol.SubscriptionMsg (Protocol.Unsubscribe (Protocol.Table table_id))
                    ]
                )

            ( ViewMsg (Right c_msg), Table table_id, Just me ) ->
                let
                    ( m_new_table, m_table_event ) =
                        model.gore.tables
                            |> Dict.get table_id
                            |> Maybe.map (TableClient.Update.update me c_msg)
                            |> Maybe.map
                                (\( model, m_table_event ) ->
                                    case m_table_event of
                                        Just table_event ->
                                            ( Just model, m_table_event )

                                        Nothing ->
                                            ( Nothing, Nothing )
                                )
                            |> Maybe.withDefault ( Nothing, Nothing )

                    tables =
                        case m_new_table of
                            Just table ->
                                model.gore.tables |> Dict.insert table_id table

                            Nothing ->
                                model.gore.tables
                in
                    ( { model
                        | gore = { tables = tables }
                      }
                    , m_table_event
                        |> Maybe.map (\m -> Protocol.GoreEvent (TableMsg table_id ( me, m )))
                        |> maybeToList
                        |> send_msgs
                    )

            _ ->
                always ( model, Cmd.none ) <| Debug.log "invalid message" ( msg, model )
