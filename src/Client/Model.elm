module Client.Model exposing (..)

import Dict
import Gore
import Protocol


type State
    = Loading
    | Lobby
    | Joining Protocol.TableId
    | Table Protocol.TableId


type alias Model =
    { user : Maybe Protocol.UserId
    , ws_addr : String
    , state : State
    , gore : Gore.GoreModel
    }


init : String -> Model
init ws_addr =
    { user = Nothing
    , ws_addr = ws_addr
    , state = Loading
    , gore =
        { tables = Dict.empty
        }
    }
