module Client.Elements exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


btn : msg -> String -> Html msg
btn msg txt =
    button [ class "btn teal darken-3", onClick msg ] [ text txt ]
