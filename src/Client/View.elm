module Client.View exposing (view)

import Dict
import Either exposing (Either(..))
import Html exposing (Html, div, span, text, button, node)
import Html.Attributes exposing (rel, href)
import Client.Model exposing (Model, State(..))
import Client.Update exposing (Msg(..))
import LobbyClient.View
import TableClient.View


view : Model -> Html Msg
view model =
    div []
        [ node "link" [ rel "stylesheet", href "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css" ] []
        , node "style" [] [ text css ]
        , case ( model.state, model.user ) of
            ( Lobby, Just user ) ->
                model.gore |> LobbyClient.View.view user |> Html.map (ViewMsg << Left)

            ( Joining table_id, Just user ) ->
                div [] [ text ("joining table " ++ table_id) ]

            ( Table table_id, Just user ) ->
                case model.gore.tables |> Dict.get table_id of
                    Just table ->
                        table
                            |> TableClient.View.view user
                            |> Html.map (Right >> ViewMsg)

                    Nothing ->
                        Html.text ("loading table " ++ table_id)

            ( _, _ ) ->
                div [] [ text "loading" ]
        ]


css : String
css =
    """
html {
    background-color: #333;
    color: white;
}
"""
