module Client.Main exposing (..)

import Navigation
import Json.Decode
import WebSocket
import Client.Model exposing (Model, init)
import Client.View exposing (view)
import Client.Update exposing (Msg(..), update)
import Protocol


ws_addr : Navigation.Location -> String
ws_addr loc =
    (if loc.protocol == "https:" then
        "wss://"
     else
        "ws://"
    )
        ++ loc.hostname
        ++ ":"
        ++ "8080"


subscriptions : Model -> Sub Msg
subscriptions model =
    WebSocket.listen model.ws_addr <| ServerMsg << Json.Decode.decodeString Protocol.jsonDecGoreEvent


main : Program Never Model Msg
main =
    Navigation.program (\loc -> Nop)
        { view = view
        , init = \loc -> ( init (ws_addr loc), Cmd.none )
        , update = update
        , subscriptions = subscriptions
        }
