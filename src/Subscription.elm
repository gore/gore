module Subscription exposing (..)

import EveryBiDict exposing (EveryBiDict)
import EveryDict exposing (EveryDict)
import Protocol


type alias ConnId =
    String


type alias SubscriptionMsg =
    ( Protocol.UserId, Protocol.SubscriptionEvent )


type alias SubscriptionModel =
    EveryBiDict Protocol.Query ConnId


init : SubscriptionModel
init =
    EveryBiDict.empty


is_take : ( Protocol.UserId, Protocol.TableEvent ) -> Bool
is_take ( user, event ) =
    case event of
        Protocol.TakeBlack ->
            True

        Protocol.TakeWhite ->
            True

        _ ->
            False



-- | Returns the list of queries that match a given message.


matching_queries : Protocol.GoreEvent -> List Protocol.Query
matching_queries msg =
    case msg of
        Protocol.TableMsg table_id table_msg ->
            [ Protocol.Table table_id ]
                ++ if is_take table_msg then
                    [ Protocol.Lobby ]
                   else
                    []

        Protocol.ChatMsg chat_id _ ->
            [ Protocol.Chat chat_id ]

        Protocol.LoginMsg user_id ->
            []


test : Protocol.Query -> Protocol.GoreEvent -> Bool
test q msg =
    case ( q, msg ) of
        ( Protocol.Lobby, Protocol.TableMsg table_id ( user, Protocol.TakeWhite ) ) ->
            True

        ( Protocol.Lobby, Protocol.TableMsg table_id ( user, Protocol.TakeBlack ) ) ->
            True

        ( Protocol.Table q_table_id, Protocol.TableMsg msg_table_id table_msg ) ->
            q_table_id == msg_table_id

        _ ->
            False


update : ConnId -> Protocol.SubscriptionEvent -> SubscriptionModel -> SubscriptionModel
update conn_id msg =
    case msg of
        Protocol.Subscribe q ->
            EveryBiDict.insert q conn_id

        Protocol.Unsubscribe q ->
            EveryBiDict.removeOne q conn_id

        Protocol.Disconnect ->
            EveryBiDict.removeBySecond conn_id


dispatch : Protocol.GoreEvent -> SubscriptionModel -> EveryDict ConnId ()
dispatch msg model =
    matching_queries msg
        |> List.map (\q -> model |> EveryBiDict.getByFirst q)
        |> List.foldr EveryDict.union EveryDict.empty
