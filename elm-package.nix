{
  "elm-community/lazy-list" = {
    version = "1.0.0";
    sha256 = "1hxpyd20rmicr6k6wwzyxhqwwbsh22ml7gr0j74vqql1k7yysshh";
  };
  "brainrape/elm-bidict" = {
    version = "1.0.0";
    sha256 = "14cgvq15hh8rj9lxyka4v0awnd8xp766ajprwvikm4hfkf9i6c6m";
  };
  "elm-community/elm-material-icons" = {
    version = "2.0.1";
    sha256 = "00591bk268gchxzjy5j7z806g5n3lb6qp08crkhrb6k9vrp4jb8p";
  };
  "elm-community/elm-test" = {
    version = "3.1.0";
    sha256 = "02rfqmlccsdadhflw14dhsfq90vl8wvy75vigg51vqhhdnnjwxal";
  };
  "elm-lang/navigation" = {
    version = "2.1.0";
    sha256 = "0xyamkcw3misjfyc523g0bh97lffrikaxk2hklwmla9i7j39ivf3";
  };
  "elm-community/shrink" = {
    version = "2.0.0";
    sha256 = "1f205dvgyqg4phf0hvk57nsxwhfhwhy5y4v6msc3fa9xgyzx7fik";
  };
  "elm-community/maybe-extra" = {
    version = "3.1.0";
    sha256 = "1gw8jbdiwklc6qav3jggj5iagy9kjqiz062krfx5xl7hf3807grs";
  };
  "toastal/either" = {
    version = "3.3.0";
    sha256 = "1fg7gh9w2piqv8blbx5acdj7pi95jn1qcjwcksvp083ra12maksp";
  };
  "elm-lang/virtual-dom" = {
    version = "2.0.4";
    sha256 = "13kc3jim6js827h91m6wx6n16qb3b6p6ms6789ks106zpwmhfwxv";
  };
  "mgold/elm-random-pcg" = {
    version = "4.0.2";
    sha256 = "1kzhbqa055mdlhxd5pcwwgqlh885vj20hwh26q1qjh63969q9cqp";
  };
  "elm-lang/lazy" = {
    version = "2.0.0";
    sha256 = "086f57n0ajdyn8rax11qp1dlym1hz3s6qv5hrmq8azw0q27byipn";
  };
  "jinjor/elm-time-travel" = {
    version = "2.0.0";
    sha256 = "1c7y98wbgfmwqksyp8qwfappaddqjlrwfcir7bv3fk4lzvrz3bkc";
  };
  "elm-lang/dom" = {
    version = "1.1.1";
    sha256 = "1y6nm3np10l34mhpc9c88vv7kc7qgaxim4cmagf4ig82rwi9mpl1";
  };
  "elm-lang/websocket" = {
    version = "1.0.2";
    sha256 = "1rqc5yl4wmfh6wvm8dhrfdw4rjdnpb08d2qwd382w49c50gm0m3z";
  };
  "elm-lang/html" = {
    version = "2.0.0";
    sha256 = "05sqjd5n8jnq4lv5v0ipcg98b8im1isnnl4wns1zzn4w5nbrjjzi";
  };
  "elm-community/json-extra" = {
    version = "2.5.0";
    sha256 = "01v8sxyivdv1j4p84m6v30q9azixp95vsi0g8y5nnaf7dhamr0a0";
  };
  "danielnarey/elm-toolkit" = {
    version = "4.5.0";
    sha256 = "0v9ppq97ijx5f6cdi2jymz6w1bjsh1jrsck8547dglw94fkf2gqg";
  };
  "bartavelle/json-helpers" = {
    version = "1.3.0";
    sha256 = "1413hcgmih0i128slc8ca4a0qpn74sq8z8nqxgr0pv2ih3hwgdcz";
  };
  "elm-community/list-extra" = {
    version = "5.0.1";
    sha256 = "1i6ghsswhi4nqgk0nb6fa2lwivr281367b80hzjaqrc3myq99kac";
  };
  "elm-lang/svg" = {
    version = "2.0.0";
    sha256 = "1c7p967n1yhynravqwgh80vprwz7r2r1n0x3icn5wzk9iaqs069l";
  };
  "eeue56/elm-all-dict" = {
    version = "2.0.1";
    sha256 = "0cl99xv3cg1hj9plmxwqha1jzwq5n12h1pnff8y6l1b6dn6la0p8";
  };
  "rtfeldman/html-test-runner" = {
    version = "2.0.0";
    sha256 = "0qrbd37l86wgh1gl54bxrn9ckij40wwi225ab3kij2gxih02dfjz";
  };
  "Skinney/elm-array-exploration" = {
    version = "2.0.5";
    sha256 = "19qlnr7p49ny9qci1i49lwm0v5j8pzflq4bca27z2jlprjrkhpcb";
  };
  "rtfeldman/node-test-runner" = {
    version = "3.0.0";
    sha256 = "1xm5dgawlxp5v9hdjwksdr8fabdcirdyyb432x19vhyx1llw419v";
  };
  "Bogdanp/elm-combine" = {
    version = "3.1.1";
    sha256 = "0wpb88an1q83pcpf21a8a73gq7a5pfn35bdiwdzf6kvgg062jfbp";
  };
  "jinjor/elm-inline-hover" = {
    version = "1.0.2";
    sha256 = "0xvmzqx7iygi5s4d5y14f7fgk9h21qnxraqjc6dzdpchmjijzywv";
  };
  "jinjor/elm-diff" = {
    version = "1.0.4";
    sha256 = "1l7f1w6sq3cqfw6wngjzachkzwwlwvjl5dgp4nbkiw5890r09maq";
  };
  "elm-lang/core" = {
    version = "5.1.1";
    sha256 = "1ndw8ffn2la48gvs7xf3zrc5d7zqrq4j6scbrb60mb2ml1a5hhry";
  };
}
