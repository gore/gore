#!/bin/sh
echo "Running CI locally using gitlab-runner with bind-mounted nix store..."
set -e
gitlab-runner exec docker \
  --docker-volumes "/nix:/nix:ro" \
  --docker-volumes "/nix/var/nix/daemon-socket/socket:/nix/var/nix/daemon-socket/socket" \
  --docker-volumes "/run/current-system/sw:/run/current-system/sw:ro" \
  --docker-volumes "/etc/ssl/certs/ca-certificates.crt:/etc/ssl/certs/ca-certificates.crt:ro" \
  --env "NIX_REMOTE=daemon" \
  --env "NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt" \
  --env "NIX_PATH=nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixpkgs" \
  build
