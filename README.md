online go


## develop

`nix-shell --run "npm start"`

*UI*: open [http://localhost:8000](http://localhost:8000)

*Tests*: open [http://localhost:8000/HtmlTests.html](http://localhost:8000/HtmlTests.html)


## build + test

`nix-shell --run "npm test"`
