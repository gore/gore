{ mkDerivation, base, elm-bridge, stdenv }:
mkDerivation {
  pname = "gore";
  version = "1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base elm-bridge ];
  license = stdenv.lib.licenses.mit;
}
