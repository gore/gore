# Nix expression defining the frontend derivation

{ pkgs ? import ./pinned-nixpkgs.nix {}, ghc ? pkgs.haskell.packages.ghc802, ...} :
let
  frontendTools = import ./frontend-tools.nix { inherit pkgs; };
  ghcWithElmBridge = ghc.ghcWithPackages (pkgs: with pkgs;
    [ elm-bridge ]);
in frontendTools.mkFrontend rec {
  name = "gore-backend";
  src = ./.;
  nodejs = pkgs."nodejs-6_x";
  node_modules = import ./node-modules-override.nix { inherit nodejs; inherit pkgs; };
  elm_packages = import ./elm-package.nix;
  buildInputs = [ frontendTools.updateFrontendDeps ghcWithElmBridge pkgs.closurecompiler pkgs.makeWrapper ];
  propagatedBuildInputs = [];
  closurecompiler = pkgs.closurecompiler;
  buildPhase = "gulp build:server";
  installPhase = ''
    mkdir -p $out/lib/node_modules/gore/out/server/
    cp -R out/server/Main.js $out/lib/node_modules/gore/out/server/
    cp -R server.js $out/lib/node_modules/gore/
    cp -R node_modules $out/lib/node_modules/gore/
    makeWrapper ${pkgs.nodejs}/bin/node $out/bin/gore-backend \
      --add-flags $out/lib/node_modules/gore/server.js
  '';

  # node_modules."elm-test-^0.18.9"
}
